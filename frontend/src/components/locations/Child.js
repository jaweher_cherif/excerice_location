import React from "react";
import Parent from "./Parent";
import "./locations.css"

class Child extends React.PureComponent {
  state = {
    name: this.props.name,
    children: this.props.children,
  };
  render() {
    var items = [];
    if (this.state.children.length == 0)
        items.push(<li key={this.state.name}> <span className="tree_label">{this.state.name}</span></li>);
    else
      items.push(
        <Parent name={this.state.name} children={this.state.children} key={this.state.name}></Parent>
      );
    return <div>{items}</div>;
  }
}

export default Child;
