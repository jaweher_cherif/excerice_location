import React from "react";
import Child from "./Child";
import "./locations.css";

class Parent extends React.Component {
  state = {
    checked: false ,
    name: this.props.name,
    children: []
  };
  handleAddChild = (e) => {
    this.setState({
        checked: e.target.checked
    });
  if(this.state.children.length===0)
    this.setState({
      children: this.props.children
    });
    else
    this.setState({
        children: [],
    });
  };
  render() {
    return (
        <li>
            <input type="checkbox"  checked={this.state.checked}  id={this.state.name} onChange={this.handleAddChild}/>
            <label className="tree_label" htmlFor={this.state.name}>{this.state.name}</label>
            <ul >
                {this.state.children.map((item) => {
                return <Child name={item.name} children={item.children} key={item.name} />;
                })}
            </ul>
        </li>
    );
  }
}

export default Parent;
