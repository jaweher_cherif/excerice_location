import React from 'react';
import { useSelector } from 'react-redux';
import './locations.css';
import Parent from "./Parent";
 

function list_to_tree(list) {
    var map = {}, node, roots = [], i;
    for (i = 0; i < list.length; i += 1) {
      list[i].name=list[i].label;
      map[list[i].id] = i;
      list[i].children = []; 
    }
    for (i = 0; i < list.length; i += 1) {
      node = list[i];
      if (node.parentId !== "NULL") {
        list[map[node.parentId]].children.push(node);
      } else {
        roots.push(node);
      }
    }
    return roots;
  }

const childrenView = (parent,items) =>{
  if(parent.children==[])
    return ;
  let child;
  for(child in parent.children){
    items.push( <li>{child.name}</li>)
    return(childrenView(child));
  }
}

const Locations =  () => {
  const locations = useSelector((state) => state.locations);
  const tree =list_to_tree(locations);
  var i;
    var items = [];
    for (i = 0; i < tree.length; i += 1) {
      items.push(
        <Parent name={tree[i].name} children={tree[i].children} key={tree[i].id}></Parent>
      );
    }
  return (<ul className="tree">{items}</ul>);
};
export default Locations;