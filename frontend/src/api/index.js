import axios from 'axios';

const url = 'http://localhost:5000/localites';

export const fetchLocations = () => axios.get(url);
export const fetchLocationById =(id) =>  axios.get(url, id);
