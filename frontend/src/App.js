import React, { useState, useEffect } from 'react';

import { useDispatch } from 'react-redux';
import { getLocations } from './actions/locations';
import Locations from './components/locations/locations';


const App = () => {
  const [currentId, setCurrentId] = useState(0);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getLocations());
  }, [currentId, dispatch]);

  return (<Locations />);
};

export default App;