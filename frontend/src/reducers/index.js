import { combineReducers } from 'redux';

import locations from './locations';

export const reducers = combineReducers({ locations });