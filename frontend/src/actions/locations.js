import * as api from '../api/index.js';

export const getLocations = () => async (dispatch) => {
  try {
    const { data } = await api.fetchLocations();
    dispatch({ type: 'FETCH_ALL', payload: data });
  } catch (error) {
    console.log(error.message);
  }
};

