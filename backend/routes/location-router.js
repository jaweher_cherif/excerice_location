const express = require('express')

const LocationCtrl = require('../controllers/location-ctrl')

const router = express.Router()

router.get('/localites', LocationCtrl.getLocations)
router.get('/localites/:id', LocationCtrl.getLocationById)

module.exports = router