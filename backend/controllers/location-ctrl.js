const Location = require ('../models/location-model.js');

const getLocations = async (req, res) => { 
    try {
        const locations = await Location.find();
                
        res.status(200).json(locations);
    } catch (error) {
        res.status(404).json({ message: error.message });
    }
}

const getLocationById = async (req, res) => { 
    const { id } = req.params;
    try {
        const location = await Location.find({'id': id });
        
        res.status(200).json(location);
    } catch (error) {
        res.status(404).json({ message: error.message });
    }
}


module.exports = {
    getLocations,
    getLocationById,
}