const mongoose = require('mongoose')
const Schema = mongoose.Schema

const Location = new Schema(
    {
        id: { type: String, required: true },
        label: { type: String, required: true },
        parentId: { type: String, required: true }
    },
    { collection : 'location' }
)

module.exports = mongoose.model('Location', Location)